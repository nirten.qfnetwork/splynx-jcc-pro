<?php

namespace app\commands;

use splynx\base\BaseInstallController;

/**
 * Class InstallController
 * @package app\commands
 */
class InstallController extends BaseInstallController
{
    /**
     * @inheritdoc
     */
    public $module_status = self::MODULE_STATUS_ENABLED;

    /**
     * @inheritdoc
     */
    public static $minimumSplynxVersion = '3.1';

    /**
     * @inheritdoc
     * @return string
     */
    public function getAddOnTitle()
    {
        // Db column varchar(64)
        return 'Splynx jcc pro';
    }

    /**
     * @inheritdoc
     */
    public function getModuleName()
    {
        // Db column varchar(32)
        return 'splynx_jcc_pro';
    }

    /**
     * @inheritdoc
     */
    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
            ],
            [
                'controller' => 'api\admin\finance\BankStatements',
                'actions' => ['index', 'view', 'add'],
            ],
            [
                'controller' => 'api\admin\finance\BankStatementsRecords',
                'actions' => ['index', 'view', 'add', 'update'],
            ],
            [
                'controller' => 'api\admin\finance\Payments',
                'actions' => ['index', 'add'],
            ],
            [
                'controller' => 'api\admin\finance\Transactions',
                'actions' => ['index', 'add'],
            ],
            [
                'controller' => 'api\admin\finance\Invoices',
                'actions' => ['index', 'view', 'update'],
            ],
            [
                'controller' => 'api\admin\finance\Requests',
                'actions' => ['index', 'view', 'update'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\administration\Partners',
                'actions' => ['index', 'view'],
            ],
//            [
//                'controller' => 'api\admin\customers\CustomerPaymentAccounts',
//                'actions' => ['index', 'view', 'update', 'delete'],
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getEntryPoints()
    {
        return [
            [
                'name' => 'jcc_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'url' => '%2Fjcc%2F',
                'icon' => 'fa-puzzle-piece',
            ],
        ];
    }

    public function actionTest()
    {
        $info = new \SplFileInfo('/var/www/splynx/uploads/backup/storage/2/2.zip');
        echo 'Size: ' . $info->getSize() . PHP_EOL;
       /* exec("echo 'DELETE FROM templates;' |" .
            "mysql -u splynx -h localhost --password=xWsV2v2NLmrJ9Mrv splynx ");
        exec('find /var/www/splynx/templates/ -type f -name "*.twig" -delete  & wait');
        exec('find /var/www/splynx/default/templates-added/ -type f -name "*.twig.installed" -delete  & wait');
        exec('find /var/www/splynx/default/templates/ -type f -name "*.twig.installed" -delete  & wait');
        exec('/usr/bin/php /var/www/splynx/system/setup/setup install-templates & wait');*/
    }
}
