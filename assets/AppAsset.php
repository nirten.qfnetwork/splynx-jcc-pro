<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class AppAsset
 * @package app\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    /**
     * @var string[]
     */
    public $css = [
        'css/site.css',
    ];
    /**
     * @var string[]
     */
    public $js = [
    ];
    /**
     * @var string[]
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'splynx\assets\HelperAsset',
    ];
    /**
     * @var array<string, int>
     */
    public $jsOptions = ['position' => View::POS_HEAD];
}
