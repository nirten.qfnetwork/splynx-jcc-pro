<?php

namespace app\controllers;

use app\models\Customer;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     * @return array<string, string|array<mixed>>
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     * @return array<string, array<string, string|array<mixed>>>
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        /** @var \yii\web\View $view */
        $view = $this->getView();
        $view->title = 'Splynx Add-on Skeleton';

        return $this->render('index', [
            'model' => new Customer()
        ]);
    }
}
