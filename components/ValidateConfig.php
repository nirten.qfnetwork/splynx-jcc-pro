<?php

namespace app\components;

use splynx\helpers\ConfigHelper;
use splynx\v2\helpers\ApiHelper;
use yii\base\Component;
use Yii\base\InvalidConfigException;
use yii\base\UserException;

/**
 * Class ValidateConfig
 * @package app\components
 */
class ValidateConfig extends Component
{
    /**
     * @return void
     * @throws UserException
     * @throws InvalidConfigException
     */
    public static function checkAll(): void
    {
        self::checkApi();
    }

    /**
     * @return void
     * @throws InvalidConfigException
     * @throws UserException
     */
    public static function checkApi(): void
    {
        if (!ConfigHelper::get('api_key')) {
            throw new UserException('Error: api_key is not set. Please check your addon config!');
        }

        if (!ConfigHelper::get('api_secret')) {
            throw new UserException('Error: api_secret is not set. Please check your addon config!');
        }

        $apiDomain = ConfigHelper::get('api_domain');
        if (!$apiDomain) {
            throw new UserException('Error: api_domain is not set. Please check your addon config!');
        }

        if (!ApiHelper::checkApi()) {
            throw new UserException('Error: Api call error. Please check your addon config!');
        }
    }
}
