<?php

namespace Helper;

use Codeception\Module;
use ReflectionClass;
use ReflectionException;

/**
 * Class Unit
 * @package Helper
 */
class Unit extends Module
{
    /**
     * @param object $object
     * @param string $methodName
     * @param array<mixed> $parameters
     * @return mixed
     * @throws ReflectionException
     */
    public function invokeMethod(object $object, string $methodName, array $parameters = [])
    {
        $reflection = new ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
